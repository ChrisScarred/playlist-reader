from dotenv import load_dotenv
import os

load_dotenv()

CONFIG = {
    "playlist_id": os.getenv("PLAYLIST_ID"),
    "data_folder": "data"
}