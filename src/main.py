from typing import Dict, List, Tuple
import spotipy
from spotipy.oauth2 import SpotifyOAuth
import src.config as config
import os
import time
import pandas as pd

def get_tracks(id: str, sp: spotipy.Spotify, n: int) -> List:
    tracklist = []
    m = 0
    while m < n:
        hits = sp.playlist_tracks(id, offset=m)
        tracks = hits["items"]
        m += len(tracks)
        tracklist.extend(tracks)
    return tracklist


def parse_artists(artists: List[Dict]) -> str:
    names = []
    for artist in artists:
        names.append(artist["name"])
    return " & ".join(names)


def query_tracks(tracklist: List, sp: spotipy.Spotify) -> List[List[str]]:
    results = [("artist", "song", "added_by")]
    for track in tracklist:
        added_by = "unknown"
        user = sp.user(track["added_by"]["id"])
        if user:
            added_by = user["display_name"]
        song = track["track"]["name"]
        artist = parse_artists(track["track"]["artists"])
        results.append((artist, song, added_by))
    return results


def save_playlist(results: List[Tuple[str]], path: str) -> None:
    df = pd.DataFrame(data=results[1:], columns=results[0])
    df.to_csv(path)

def main(config: Dict) -> None:
    scope = "user-library-read"
    sp = spotipy.Spotify(auth_manager=SpotifyOAuth(scope=scope))
    id = config.get("playlist_id")
    playlist = sp.playlist(id)
    if playlist:
        name = playlist["name"]
        tracks = get_tracks(id, sp, playlist["tracks"]["total"])
        results = query_tracks(tracks, sp)
        save_playlist(results, os.path.join(config.get("data_folder", ""), name.replace(" ", "-") + "-" + str(time.time()).replace(".", "-") + ".csv"))


if __name__=="__main__":
    main(config.CONFIG)
