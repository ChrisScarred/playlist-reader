# Spotify Playlist Reader

Reads song parameters of interest from a Spotify playlist.

## Instructions

1. Install dependencies specified in `pyproject.toml` (poetry) or `requirements.txt` (pip), preferably through a virtual environment.
2. Copy `example.env` into `.env` and adjust placeholders with applicable values.
3. Adjust config values in `config.py` if necessary.
4. Run via `[poetry run] python -m src.main`.
